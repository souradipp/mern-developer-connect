const express = require('express');
const connectDB = require('./config/dbConnection')
const app = express();

connectDB();

app.use("/api/users", require('./routes/api/user'))
app.use("/api/posts", require('./routes/api/posts'))
app.use("/api/profile", require('./routes/api/profile'))
app.use("/api/auth", require('./routes/api/auth'))

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => { console.log("server started") })